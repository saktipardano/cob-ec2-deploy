package main
import (
        "fmt"
        "net/http"
)
func hello(w http.ResponseWriter, req *http.Request) {
        fmt.Fprintf(w, "hello Sakti Pardano\n")
}
func main() {
        http.HandleFunc("/", hello)
        fmt.Println(`listen at :8000`)
        fmt.Println(http.ListenAndServe(":8000", nil))
}

